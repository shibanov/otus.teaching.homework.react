import React from 'react';
import {Link} from 'react-router-dom';

export default function NotFound() {

    return (
        <div className="not-found">
            <h2>This page not found</h2>
            <h1>404</h1>
            <Link to="/">Go Home</Link>
        </div>
    );
}