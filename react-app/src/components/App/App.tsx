import React from 'react';
import {BrowserRouter as Router, Switch} from 'react-router-dom';
import {Route} from 'react-router';
import Layout from '../Layout/Layout';
import Home from '../Home/Home';
import Counter from '../Counter/Counter';
import Dashboard from '../Dashboard/Dashboard';
import Preferences from '../Preferences/Preferences';
import NotFound from './NotFound';
import './App.css';

export default function App() {
    return (
        <Router>
            <Layout>
                <Switch>
                    <Route exact path='/' component={Home}/>
                    <Route exact path='/counter' component={Counter}/>
                    <Route exact path='/dashboard' component={Dashboard}/>
                    <Route exact path='/preferences' component={Preferences}/>
                    <Route component={NotFound}/>
                </Switch>
            </Layout>
        </Router>
    );
}