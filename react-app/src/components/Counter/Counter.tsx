import React, {useState} from 'react'
import {useSelector, useDispatch} from 'react-redux'
import {decrement, increment, incrementByAmount, selectCount} from './counterSlice'
import useToken from '../App/useToken';
import Login from '../Login/Login';
import styles from './Counter.module.css'

export default function Counter() {
    const {token, setToken} = useToken();

    const count = useSelector(selectCount)
    const dispatch = useDispatch()
    const [incrementAmount, setIncrementAmount] = useState('2')
    const incrementValue = Number(incrementAmount) || 0;

    if (!token) {
        return <Login setToken={setToken}/>
    }

    return (
        <div>
            <h2>Counter</h2>
            <div className={styles.row}>
                <button
                    className={styles.button}
                    aria-label="Increment value"
                    onClick={() => dispatch(increment())}
                >
                    +
                </button>
                <span className={styles.value}>{count}</span>
                <button
                    className={styles.button}
                    aria-label="Decrement value"
                    onClick={() => dispatch(decrement())}
                >
                    -
                </button>
            </div>
            <div className={styles.row}>
                <input
                    className={styles.textbox}
                    aria-label="Set increment amount"
                    value={incrementAmount}
                    onChange={(e) => setIncrementAmount(e.target.value)}
                />
                <button
                    className={styles.button}
                    onClick={() => dispatch(incrementByAmount(incrementValue))}
                >
                    Add Amount
                </button>
            </div>
        </div>
    )
}