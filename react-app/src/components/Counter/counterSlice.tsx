import {createSlice} from '@reduxjs/toolkit'
import {RootState} from '../App/store';

const setStorage = (state: any) => {
    sessionStorage.setItem('counter/state', JSON.stringify(state));
};
const getStorage = () => {
    const stateString = sessionStorage.getItem('counter/state');
    return JSON.parse(stateString!) ||
        {
            value: 0
        }
};

export const counterSlice = createSlice({
    name: 'counter',
    initialState: getStorage(),
    reducers: {
        increment: state => {
            // Redux Toolkit allows us to write "mutating" logic in reducers. It
            // doesn't actually mutate the state because it uses the immer library,
            // which detects changes to a "draft state" and produces a brand new
            // immutable state based off those changes
            state.value += 1
            setStorage(state);
        },
        decrement: state => {
            state.value -= 1
            setStorage(state);
        },
        incrementByAmount: (state, action) => {
            state.value += action.payload
            setStorage(state);
        }
    }
})

export const {increment, decrement, incrementByAmount} = counterSlice.actions

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectCount = (state: RootState) => state.counter.value;

export default counterSlice.reducer